import pickle
import numpy as np
import json

# Così leggo il file: FATTO
# with open('/home/maugeri/ojvs-skills/Paper/JD2Skills-BERT-XMLC-main/pybert/dataset/job_dataset.train.pkl', 'rb') as file:
#     train = pickle.load(file)
#     print(type(train))

# with open('prova_train.json', 'w', encoding='utf-8') as f:
#     json.dump(train, f, ensure_ascii=False, indent=4)

# with open('prova_train.json', 'w', encoding='utf-8') as f:
#     f.write(json.dumps(train, ensure_ascii=False))

#print(str(train[:2]))


# Così salvo ogni tupla in una riga di testo: (e posso poi reimportarla e renderla di nuovo lista) FATTO
# with open("/home/maugeri/ojvs-skills/Paper/JD2Skills-BERT-XMLC-main/pybert/dataset/train_txt.txt", 'w') as output:
#     for elemento in train:
#         output.write(str(elemento) + '\n')


#Devo importare il .txt e renderlo .pkl
# file1=open('/home/maugeri/ojvs-skills/Paper/JD2Skills-BERT-XMLC-main/pybert/dataset/train_txt.txt','r')
# obj=file1.read().encode('ascii', errors='ignore').decode('utf-8')
# pickle.dump(obj,open('/home/maugeri/ojvs-skills/Paper/JD2Skills-BERT-XMLC-main/pybert/dataset/train2.pkl','wb'))


with open('/home/maugeri/ojvs-skills/Paper/JD2Skills-BERT-XMLC-main/pybert/dataset/train_txt.txt', 'r') as f:
    text = f.read()

text=text.encode(encoding = 'UTF-8', errors = 'ignore')

with open('/home/maugeri/ojvs-skills/Paper/JD2Skills-BERT-XMLC-main/pybert/dataset/train_utf8.txt', 'wb') as file:
    file.write(text)

file1=open('/home/maugeri/ojvs-skills/Paper/JD2Skills-BERT-XMLC-main/pybert/dataset/train_utf8.txt','r')
obj=file1.read()
pickle.dump(obj, open('/home/maugeri/ojvs-skills/Paper/JD2Skills-BERT-XMLC-main/pybert/dataset/job_dataset.train.pkl','wb'))

