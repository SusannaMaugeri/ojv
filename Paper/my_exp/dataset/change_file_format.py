import pickle
import numpy as np
import json

# Così leggo il file:
with open('/home/maugeri/ojvs-skills/Paper/JD2Skills-BERT-XMLC-main/pybert/dataset/job_dataset.train.pkl', 'rb') as file:
    train = pickle.load(file)
print(type(train))
print(train[:1]) #lista di tuple di dimensione 2

file_testo=open('/home/maugeri/ojvs-skills/Paper/my_exp/dataset/corpus_train.csv', 'w')
file_target=open('/home/maugeri/ojvs-skills/Paper/my_exp/dataset/vettori_target.csv', 'w')

for elemento in train:
    stringa=elemento[0]
    vettore=elemento[1]
    file_testo.write(stringa + '\n')
    file_target.write(str(vettore.tolist()) + '\n')

file_testo.close()
file_target.close()