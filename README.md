# OJVs Skills

- pwd: mostra il percorso dove sei
- ls: mostra i file presenti nella cartella
- rm -rf (cartella o file): cancella la cartella e tutto quello che c'è dentro (pericoloso)
- git clone (url): scarica tutta la repository da git
- git add nomefile: aggiunge il file allo staging (-A per tutti i file)
- git commit: committa -> scrivi il messaggio -> ctrl x per salvare, yes e invio
- git push: pusha il commit al server
- git pull: prende i commit che non hai
- git status: ti fa vedere quali file sono cambiati


## Virtualenv
- python3 -m virtualenv venv -> crea l'environment
- source venv/bin/activate -> entri dentro
- pip3 install <> installi i pacchetti
- pip3 freeze > requirements .txt  -> scrivi i pacchetti in un file di testo
- pip3 install -r requirements.txt  -> reinstalli i pacchetti dal file di testo scritto
